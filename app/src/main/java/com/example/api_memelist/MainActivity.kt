package com.example.api_memelist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.api_memelist.databinding.ActivityMainBinding
import com.example.api_memelist.fragments.RecyclerViewFragment

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, RecyclerViewFragment())
            setReorderingAllowed(true)
            addToBackStack("name") // name can be null
            commit()
        }

    }
}