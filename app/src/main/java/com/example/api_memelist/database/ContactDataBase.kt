package com.example.api_memelist.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.api_memelist.database.entitys.FavouriteEntity

@Database(entities = arrayOf(FavouriteEntity::class), version = 1)
abstract class ContactDataBase: RoomDatabase() {
    abstract fun contactDao(): ContactDao
}