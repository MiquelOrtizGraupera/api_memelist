package com.example.api_memelist.database.entitys

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "FavouriteEntity")
data class FavouriteEntity(
    @PrimaryKey (autoGenerate = true) var id : Long = 0,
    var name: String,
    var image: String
)
