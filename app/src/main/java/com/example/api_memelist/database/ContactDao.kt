package com.example.api_memelist.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.api_memelist.database.entitys.FavouriteEntity

@Dao
interface ContactDao {
    @Query("SELECT * FROM FavouriteEntity")
    fun getAllFavouriteMemes(): MutableList<FavouriteEntity>
    @Query("SELECT count(*) FROM FavouriteEntity WHERE name = :nom")
    fun getIfExist(nom:String): Int
    @Insert
    fun addContact(favouriteEntity: FavouriteEntity)
    @Query ("DELETE FROM FavouriteEntity WHERE name = :nom")
    fun deleteMeme(nom:String)
}