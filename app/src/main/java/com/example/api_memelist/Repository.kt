package com.example.api_memelist

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.api_memelist.api.ApiInterface
import com.example.api_memelist.dataclass.Data
import com.example.api_memelist.dataclass.MemeList
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository {
    private val apiInterface = ApiInterface.create()
    suspend fun getAllMemes() = apiInterface.getData()
}
   /* var dataInfo = MutableLiveData<Data?>()

    fun fetchData() {
        CoroutineScope(Dispatchers.IO).launch {
            val response = apiInterface.getData()
            withContext(Dispatchers.Main){
                if(response.isSuccessful){
                    dataInfo.postValue(response.body())
                }
                else{
                    Log.e("Error: ", response.message())
                }
            }
        }
    }

}*/


   /* fun fetchData(): MutableLiveData<MemeList> {
        val call = apiInterface.getData()
        call.enqueue(object: Callback<Data> {
            @SuppressLint("NullSafeMutableLiveData")
            override fun onFailure(call: Call<Data>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
                dataInfo.postValue(null)
            }
            override fun onResponse(call: Call<Data?>, response: Response<Data?>) {
                if (response != null && response.isSuccessful) {
                    dataInfo.value = response.body()?.data
                }
            }
        })
        return dataInfo
    }
}*/