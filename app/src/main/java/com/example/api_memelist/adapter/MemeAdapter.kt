package com.example.api_memelist.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.api_memelist.OnClickListener
import com.example.api_memelist.R
import com.example.api_memelist.databinding.ItemMemeBinding
import com.example.api_memelist.dataclass.Meme
import com.example.api_memelist.dataclass.MemeList

class MemeAdapter(private val memes: MemeList, private val listener:OnClickListener): RecyclerView.Adapter<MemeAdapter.ViewHolder>() {
    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemMemeBinding.bind(view)

        fun setListener(meme: Meme){
            binding.root.setOnClickListener {
                listener.onClick(meme)
            }
        }
    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_meme,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val meme = memes.memes[position]
        with(holder){
            setListener(meme)
            binding.itemTextView.text = meme.name
           // binding.itemImageView.contentDescription = meme.url // -> WHATCH NOT WORKING
            Glide.with(context)
                .load(meme.url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                //.circleCrop()
                .into(binding.itemImageView)

        }
    }

    override fun getItemCount(): Int {
        return memes.memes.size
    }
}
