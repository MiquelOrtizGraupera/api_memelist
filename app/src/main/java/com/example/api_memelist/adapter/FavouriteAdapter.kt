package com.example.api_memelist.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.api_memelist.R
import com.example.api_memelist.database.entitys.FavouriteEntity
import com.example.api_memelist.databinding.ItemMemeBinding

class FavouriteAdapter(private val favouriteMeme: MutableList<FavouriteEntity>): RecyclerView.Adapter<FavouriteAdapter.ViewHolder>() {
inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
    val binding = ItemMemeBinding.bind(view)
    }

    private lateinit var context:Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_meme,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder){
            binding.itemTextView.text = favouriteMeme[position].name
            Glide.with(context)
                .load(favouriteMeme[position].image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(binding.itemImageView)
        }
    }

    override fun getItemCount(): Int {
        return favouriteMeme.size
    }
}
