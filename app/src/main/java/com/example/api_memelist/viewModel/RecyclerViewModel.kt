package com.example.api_memelist.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.api_memelist.ContactAplication
import com.example.api_memelist.Repository
import com.example.api_memelist.database.entitys.FavouriteEntity
import com.example.api_memelist.dataclass.Data
import com.example.api_memelist.dataclass.Meme
import kotlinx.coroutines.*

class RecyclerViewModel: ViewModel() {
    private val repository = Repository()
    var data = MutableLiveData<Data?>()
    var selectedMeme = MutableLiveData<Meme>()

    var favListModel = mutableListOf<FavouriteEntity>()
    var existe:Int = 0


    init{
        fetchData()
        getItemToFavouriteList()
    }

    private fun fetchData(){
       viewModelScope.launch {
           val response = withContext(Dispatchers.IO){
               repository.getAllMemes()
           }
           if(response.isSuccessful){
               data.postValue(response.body())
           }
           else{
               Log.e("Error :", response.message())
           }
       }

    }

    fun select(meme: Meme){
        selectedMeme.postValue(meme)
    }

    private fun getItemToFavouriteList(): MutableList<FavouriteEntity> {
        CoroutineScope(Dispatchers.Main).launch {
             favListModel = withContext(Dispatchers.IO){
                ContactAplication.dataBase.contactDao().getAllFavouriteMemes()
            }
           Log.d("Base de dades carregada", favListModel.toString())
       }
        return favListModel
    }

}