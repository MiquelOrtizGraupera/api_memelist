package com.example.api_memelist.dataclass

data class Meme(
    val id: String,
    val name: String,
    val url: String
)