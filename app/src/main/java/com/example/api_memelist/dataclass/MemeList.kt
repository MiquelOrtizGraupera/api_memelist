package com.example.api_memelist.dataclass

data class MemeList(
    val memes: List<Meme>
)