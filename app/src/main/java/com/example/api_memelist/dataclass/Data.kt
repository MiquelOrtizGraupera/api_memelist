package com.example.api_memelist.dataclass

data class Data(
    val data: MemeList,
    val success: Boolean
)