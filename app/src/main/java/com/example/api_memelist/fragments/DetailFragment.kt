package com.example.api_memelist.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.api_memelist.ContactAplication
import com.example.api_memelist.R
import com.example.api_memelist.database.entitys.FavouriteEntity
import com.example.api_memelist.databinding.FragmentDetailBinding
import com.example.api_memelist.dataclass.Meme
import com.example.api_memelist.viewModel.RecyclerViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class DetailFragment : Fragment() {

    private val viewModel: RecyclerViewModel by activityViewModels()
    lateinit var binding: FragmentDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
       savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.selectedMeme.observe(viewLifecycleOwner, Observer{
            val meme= it
            Glide.with(requireContext())
                .load((meme as Meme).url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(binding.imageDetail)
            binding.textDetail.text = meme.name
            getNamesAndCompare(binding.textDetail.text.toString())
        })



        binding.favouriteStar.setOnClickListener {
            println("CLICKED! " + viewModel.existe )
            if(viewModel.existe > 0){
                deleteFromRoom(binding.textDetail.text.toString())
            }
            else{
                val name = binding.textDetail.text.toString().trim()
                val image = binding.imageDetail.toString().trim()
                val newFavourite = FavouriteEntity(name = name, image = image)
                insertFavourite(newFavourite)
                parentFragmentManager.beginTransaction().apply {
                    replace(R.id.fragmentContainerView, RecyclerViewFragment())
                    setReorderingAllowed(true)
                    addToBackStack(null)
                    commit()
                }
            }
            viewModel.existe = 0
        }
    }

    private fun insertFavourite(favouriteEntity: FavouriteEntity){
        CoroutineScope(Dispatchers.IO).launch {
            ContactAplication.dataBase.contactDao().addContact(favouriteEntity)
        }
    }
    @SuppressLint("SuspiciousIndentation")
    private fun getMemesFav(){
        CoroutineScope(Dispatchers.IO).launch {
        var memesList = ContactAplication.dataBase.contactDao().getAllFavouriteMemes()
            memesList.forEach {
                println(it)
            }
        }
    }

    private fun getNamesAndCompare(name:String){
         CoroutineScope(Dispatchers.IO).launch {
           var nameList = ContactAplication.dataBase.contactDao().getIfExist(name)
             if(nameList > 0){
                 println("EXISTE EN LA BBDD DE ROOM")
                 println("Numero retornado de la consulta " + nameList)
                 viewModel.existe = nameList
                 binding.favouriteStar.setImageResource(R.drawable.delete)
             }
        }
    }
    private fun deleteFromRoom(name:String){
        CoroutineScope(Dispatchers.IO).launch {
            ContactAplication.dataBase.contactDao().deleteMeme(name)
            println("ELIMINADO DE LA BBDD ROOM")
        }
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, RecyclerViewFragment())
            setReorderingAllowed(true)
            addToBackStack(null)
            commit()
        }
    }
}