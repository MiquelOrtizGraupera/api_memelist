package com.example.api_memelist.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.api_memelist.ContactAplication
import com.example.api_memelist.adapter.FavouriteAdapter
import com.example.api_memelist.database.entitys.FavouriteEntity
import com.example.api_memelist.databinding.FragmentFavouriteBinding
import com.example.api_memelist.viewModel.RecyclerViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FavouriteFragment : Fragment() {
    private val viewModel: RecyclerViewModel by activityViewModels()
    private lateinit var binding: FragmentFavouriteBinding
    private lateinit var  favAdapter : FavouriteAdapter
    private lateinit var linearLayout: RecyclerView.LayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentFavouriteBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        linearLayout = LinearLayoutManager(context)
        CoroutineScope(Dispatchers.IO).launch {
          setUpRecyclerView(ContactAplication.dataBase.contactDao().getAllFavouriteMemes())
        }
    }

    private fun setUpRecyclerView(myData: MutableList<FavouriteEntity>){
        favAdapter = FavouriteAdapter(myData)
        binding.favouriteRecycler.apply {
            layoutManager = linearLayout
            adapter = favAdapter
        }
    }
}