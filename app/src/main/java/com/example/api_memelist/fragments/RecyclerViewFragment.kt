package com.example.api_memelist.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.api_memelist.OnClickListener
import com.example.api_memelist.R
import com.example.api_memelist.adapter.MemeAdapter
import com.example.api_memelist.databinding.FragmentRecyclerViewBinding
import com.example.api_memelist.dataclass.Data
import com.example.api_memelist.dataclass.Meme
import com.example.api_memelist.viewModel.RecyclerViewModel
import kotlinx.coroutines.CoroutineScope

class RecyclerViewFragment : Fragment(), OnClickListener {

    private val viewModel: RecyclerViewModel by activityViewModels()
    private lateinit var binding: FragmentRecyclerViewBinding
    private lateinit var memeAdapter: MemeAdapter
    private lateinit var linearLayout: RecyclerView.LayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentRecyclerViewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Aplicar observer
        viewModel.data.observe(viewLifecycleOwner, Observer {
            if(viewModel.data.value == null){
                println("DATA BY OBSERVER IS NULL")
            }else{
                setUpRecyclerView(viewModel.data.value!!)
            }
        })
        //memeAdapter = MemeAdapter(viewModel.data.value!!,this)
        linearLayout = LinearLayoutManager(context)

        binding.irFavoritos.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FavouriteFragment())
                setReorderingAllowed(true)
                addToBackStack(null)
                commit()
            }
        }


    }

    override fun onClick(meme: Meme) {
        viewModel.select(meme)
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, DetailFragment())
            setReorderingAllowed(true)
            addToBackStack(null)
            commit()
        }
    }


     private fun setUpRecyclerView(myData: Data){
        memeAdapter = MemeAdapter(myData.data, this)
        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayout
            adapter = memeAdapter
        }
    }

}