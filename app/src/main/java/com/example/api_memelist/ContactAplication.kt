package com.example.api_memelist

import android.app.Application
import androidx.room.Room
import com.example.api_memelist.database.ContactDataBase

class ContactAplication: Application() {
    companion object {
        lateinit var dataBase: ContactDataBase
    }

    override fun onCreate() {
        super.onCreate()
        dataBase = Room.databaseBuilder(this,
            ContactDataBase::class.java, "ContactDataBase").build()
    }
}