package com.example.api_memelist.api

import com.example.api_memelist.dataclass.Data
import com.example.api_memelist.dataclass.MemeList
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiInterface {
    @GET("get_memes")
    suspend fun getData(): Response<Data>

    companion object {
        val BASE_URL = "https://api.imgflip.com/"

        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }



}