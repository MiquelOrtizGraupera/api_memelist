package com.example.api_memelist

import com.example.api_memelist.dataclass.Meme

interface OnClickListener {
    fun onClick(meme: Meme)
}